# frozen_string_literal: true

namespace :webpush do
  desc 'Genera las llaves VAPID'
  task vapid: :environment do
    vapid = Webpush.generate_key

    puts 'Agrega lo siguiente en `rails credentials:edit`:'
    puts 'vapid:'
    puts "  public_key: #{vapid.public_key}"
    puts "  private_key: #{vapid.private_key}"

    exit 0
  end
end
