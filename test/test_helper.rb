# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require 'database_cleaner'

class ActiveSupport::TestCase
  include FactoryBot::Syntax::Methods

  DatabaseCleaner.strategy = :transaction

  setup { DatabaseCleaner.start }
  teardown { DatabaseCleaner.clean }
end
