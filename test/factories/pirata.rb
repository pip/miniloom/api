# frozen_string_literal: true

FactoryBot.define do
  factory :pirata do
    email { SecureRandom.hex + '@partidopirata.com.ar' }
    nick { SecureRandom.hex }
    password { '12345678' }
  end
end
