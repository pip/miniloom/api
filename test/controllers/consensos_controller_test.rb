# frozen_string_literal: true

class ConsensosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @barca = create :barca
    @pirata = create :pirata
    @auth = { Authorization: ActionController::HttpAuthentication::Basic
            .encode_credentials(@pirata.email, @pirata.password) }
  end

  test 'se pueden mostrar' do
    2.times do
      create :consenso, barca: @barca
    end

    get barca_consensos_url(@barca), as: :json, headers: @auth
    body = JSON.parse(@response.body)

    assert_equal 200, @response.status
    assert_equal 2, body['consensos'].size
  end

  test 'al mostrar solo se ven las posiciones resumidas' do
    2.times do
      consenso = create :consenso, barca: @barca
      pirata = create :pirata

      3.times do
        create :posicion, consenso: consenso, pirata: pirata
      end
    end

    get barca_consensos_url(@barca), as: :json, headers: @auth
    body = JSON.parse(@response.body)

    body['consensos'].each do |consenso|
      assert_equal 1, consenso['posiciones'].count
    end
  end

  test 'se puede ver uno solo' do
    consenso = create :consenso, con_posiciones: 2

    get barca_consenso_url(consenso.barca, consenso),
        as: :json,
        headers: @auth
    body = JSON.parse(@response.body)

    assert_equal 200, @response.status
    assert_equal consenso.titulo, body['titulo']
    assert_equal 2, body['posiciones'].size
  end

  test 'al ver uno solo se ve todo el historial de posiciones' do
    consenso = create :consenso, barca: @barca
    pirata = create :pirata

    3.times do
      create :posicion, consenso: consenso, pirata: pirata
    end

    get barca_consenso_url(@barca, consenso), as: :json, headers: @auth
    body = JSON.parse(@response.body)

    assert_equal 3, body['posiciones'].size
  end

  test 'se pueden crear' do
    post barca_consensos_url(@barca), as: :json, headers: @auth,
                                      params: { consenso: { titulo: 'hola', texto: 'chau' } }

    assert_equal 201, @response.status

    body = JSON.parse(@response.body)
    consenso = Consenso.find(body['id'])

    assert_equal 'hola', consenso.titulo
    assert_equal 'chau', consenso.texto
  end

  test 'se pueden borrar si están vacíos' do
    consenso = create :consenso, barca: @barca

    delete barca_consenso_url(@barca, consenso),
           as: :json, headers: @auth

    assert_equal 200, @response.status

    body = JSON.parse(@response.body)
    assert_raise(ActiveRecord::RecordNotFound) { Consenso.find(body['id']) }
  end

  test 'no se pueden borrar si no están vacíos' do
    consenso = create :consenso, barca: @barca
    create :posicion, consenso: consenso

    delete barca_consenso_url(@barca, consenso),
           as: :json, headers: @auth

    assert_equal 422, @response.status
  end
end
