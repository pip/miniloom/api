# frozen_string_literal: true

require 'test_helper'
class PiratasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pirata = create :pirata
    @auth = { Authorization: ActionController::HttpAuthentication::Basic
            .encode_credentials(@pirata.email, @pirata.password) }
  end

  test 'se pueden crear' do
    subscription = build :webpush_subscription

    post webpush_subscriptions_url, as: :json, headers: @auth,
                                    params: {
                                      webpush_subscription: {
                                        auth: subscription.auth,
                                        p256dh: subscription.p256dh,
                                        endpoint: subscription.endpoint
                                      }
                                    }

    assert_equal 201, @response.status
  end

  test 'no se pueden crear duplicadas' do
    subscription = create :webpush_subscription

    post webpush_subscriptions_url, as: :json, headers: @auth,
                                    params: {
                                      webpush_subscription: {
                                        auth: subscription.auth,
                                        p256dh: subscription.p256dh,
                                        endpoint: subscription.endpoint
                                      }
                                    }

    assert_equal 204, @response.status
  end

  test 'se puede obtener la llave pública VAPID' do
    get webpush_subscriptions_vapid_public_key_url,
        as: :json, headers: @auth

    body = JSON.parse response.body

    assert body['vapid'].present?
    assert body['vapid']['public_key'].present?
  end
end
