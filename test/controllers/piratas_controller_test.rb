# frozen_string_literal: true

require 'test_helper'

class PiratasControllerTest < ActionDispatch::IntegrationTest
  test 'se pueden registrar' do
    post piratas_url, as: :json, params: {
      pirata: {
        nick: 'rene',
        email: 'rene@partidopirata.com.ar',
        password: '12345678'
      }
    }
    body = JSON.parse @response.body
    pirata = Pirata.find_by_nick(body['nick'])

    assert_equal 'rene@partidopirata.com.ar', pirata.email
    assert_equal pirata, pirata.authenticate('12345678')
  end

  test 'no se pueden registrar dos veces' do
    pirata = create :pirata
    post piratas_url, as: :json, params: {
      pirata: {
        nick: pirata.nick,
        email: pirata.email,
        password: '12345678'
      }
    }
    body = JSON.parse @response.body
    assert_equal %w[errors], body.keys
  end

  test 'puedo ver mi perfil' do
    pirata = create :pirata

    get piratas_yo_url, as: :json, headers: {
      Authorization: ActionController::HttpAuthentication::Basic
        .encode_credentials(pirata.email, pirata.password)
    }

    body = JSON.parse @response.body
    assert_equal pirata.id, body['id']
    assert_equal pirata.email, body['email']
    assert_equal pirata.nick, body['nick']
  end
end
