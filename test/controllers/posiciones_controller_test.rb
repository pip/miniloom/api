# frozen_string_literal: true

require 'test_helper'

class PosicionesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @barca = create :barca
    @pirata = create :pirata
    @auth = { Authorization: ActionController::HttpAuthentication::Basic
            .encode_credentials(@pirata.email, @pirata.password) }
  end

  test 'se pueden crear' do
    consenso = create :consenso, barca: @barca
    estado = Posicion::ESTADOS.sample

    post barca_consenso_posiciones_url(@barca, consenso),
         as: :json, headers: @auth,
         params: { posicion: { estado: estado, comentario: 'porque me place' } }

    assert_equal 201, @response.status

    body = JSON.parse(@response.body)
    posicion = Posicion.find(body['id'])

    assert_equal estado, posicion.estado
    assert_equal consenso, posicion.consenso
    assert_not_equal consenso.updated_at, posicion.consenso.updated_at
    assert_equal 'porque me place', posicion.comentario
  end
end
