# frozen_string_literal: true

require 'test_helper'

class WebpushJobTest < ActiveJob::TestCase
  test 'se pueden enviar a todas las piratas' do
    payload = WebpushPayload.new(subject: 'test', message: 'test', endpoint: 'https://partidopirata.com.ar')

    assert_nothing_raised do
      WebpushJob.perform_now(piratas: 'all', payload: payload.to_json,
                             urgency: :low, ttl: 1.day.to_i)
    end
  end

  test 'se pueden enviar a algunas piratas' do
    pirata = create :pirata
    payload = WebpushPayload.new(subject: 'test', message: 'test', endpoint: 'https://partidopirata.com.ar')

    assert_nothing_raised do
      WebpushJob.perform_now(piratas: [pirata.id], payload: payload.to_json,
                             urgency: :low, ttl: 1.day.to_i)
    end
  end
end
