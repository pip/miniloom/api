# frozen_string_literal: true

require 'test_helper'

class PosicionTest < ActiveSupport::TestCase
  test 'se pueden crear' do
    posicion = create :posicion

    assert_equal true, posicion.valid?
  end

  test 'se pueden saber las últimas' do
    consenso = create :consenso
    piratas = []
    2.times do
      piratas.push create :pirata
    end

    5.times do
      create :posicion,
             pirata: piratas.sample,
             consenso: consenso,
             created_at: (1..100).to_a.sample.days.ago
    end

    posicion1 = create :posicion, pirata: piratas.first, consenso: consenso
    posicion2 = create :posicion, pirata: piratas.last, consenso: consenso

    assert_equal [posicion1.id, posicion2.id].sort,
                 consenso.posiciones.ultimas.collect(&:id).sort
  end
end
