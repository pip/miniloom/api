# frozen_string_literal: true

class BarcaTest < ActiveSupport::TestCase
  test 'se pueden crear' do
    barca = create :barca

    assert_equal true, barca.valid?
  end

  test 'pueden tener tripulacion' do
    barca = create :barca, tripulacion: 3

    assert_equal 3, barca.tripulaciones.count
    assert_equal 3, barca.piratas.count
  end

  test 'el nombre tiene que ser único' do
    create :barca, nombre: 'hola'

    barca = build :barca, nombre: 'hola'

    assert barca.invalid?
  end

  test 'al eliminar la barca se elimina su tripulación' do
    barca = create :barca, tripulacion: 3

    assert barca.destroy
    assert_equal 3, Pirata.all.count
    assert Tripulacion.all.count.zero?
  end
end
