# frozen_string_literal: true

class WebpushSubscriptionTest < ActiveSupport::TestCase
  test 'se pueden crear' do
    subscription = create :webpush_subscription

    assert subscription.valid?
  end

  test 'no se pueden crear duplicadas' do
    subscription = create :webpush_subscription
    subscription2 = build :webpush_subscription,
                          pirata: subscription.pirata,
                          auth: subscription.auth,
                          p256dh: subscription.p256dh,
                          endpoint: subscription.endpoint

    assert subscription2.invalid?
  end
end
