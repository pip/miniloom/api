# frozen_string_literal: true

# Envía las notificaciones
class WebpushJob < ApplicationJob
  queue_as :default

  # @param :piratas [Symbol,Array] :all para traer todas, array de IDs para filtrar
  # @param :payload [WebpushPayload] La notificación a enviar
  # @param :urgency [Symbol] La urgencia
  # @param :ttl [Any] Duración convertible a segundos
  def perform(piratas:, payload:, urgency: :normal, ttl: 1.day.to_i)
    # Encontrar todas las piratas
    piratas = find_piratas(piratas)
    payload = WebpushPayload.from_json payload

    # No cargar todas las piratas en memoria!
    piratas.find_each do |pirata|
      # Enviarles a todas sus suscripciones
      pirata.webpush_subscriptions.find_each do |subscription|
        subscription.payload_send(payload: payload,
                                  urgency: urgency,
                                  ttl: ttl)
      rescue Webpush::ExpiredSubscription
        subscription.destroy
      end
    end
  end

  private

  def find_piratas(ids)
    if ids == :all
      Pirata.all
    else
      Pirata.where(id: ids)
    end
  end
end
