# frozen_string_literal: true

json.call(consenso, :id, :created_at, :updated_at, :titulo, :texto)

json.pirata consenso.pirata, partial: 'piratas/pirata', as: :pirata

ultimas ||= false
posiciones = consenso.posiciones
posiciones = posiciones.ultimas if ultimas

json.posiciones posiciones, partial: 'posiciones/posicion', as: :posicion

json.eliminable consenso.posiciones.count.zero?
