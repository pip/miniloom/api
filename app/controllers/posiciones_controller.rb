# frozen_string_literal: true

# API para posiciones
#
# Las posiciones solo se pueden agregar a su consenso
#
# @see PiratasController#create
class PosicionesController < ApplicationController
  # Necesitamos autenticarnos
  before_action :authenticate!
  before_action :find_barca!

  # POST /barcas/:barca_id/consensos/:consenso_id/posiciones
  #
  # Crea una posición dentro de un consenso
  #
  # @param :consenso_id [Integer] El ID del consenso
  # @param :posicion [Hash] { posicion: { estado: @string,
  #                                       comentario: @string } }
  # @return [Hash] { id: @int, estado: @string, comentario: @string,
  #                  pirata: @pirata }
  def create
    find_consenso!

    @posicion = @consenso.try(:posiciones).try(:build, posicion_params)
    @posicion.try(:pirata=, current_pirata)

    if @posicion.try(:save)
      @consenso.touch

      notify subject: :create
      render status: :created
    else
      render json: @posicion.try(:errors).try(:messages),
             status: :unprocessable_entity
    end
  end

  private

  def posicion_params
    params.require(:posicion).permit(:estado, :comentario)
  end

  def find_barca!
    @barca = Barca.find(params[:barca_id])
  end

  def find_consenso!
    @consenso = @barca.consensos.find(params[:consenso_id])
  end

  def get_subject(view)
    I18n.t("posiciones.#{view}.subject",
           barca: @barca.nombre,
           consenso: @consenso.titulo)
  end

  def get_posicion(estado)
    I18n.t("pirata.posiciones.tercera_persona.#{estado}")
  end

  def get_message(view)
    I18n.t("posiciones.#{view}.message",
           consenso: @consenso.texto[0..140],
           posicion: get_posicion(@posicion.estado),
           comentario: @posicion.comentario,
           nick: current_pirata.nick)
  end

  def get_endpoint(_)
    barca_consenso_path(@barca, @consenso)
  end
end
