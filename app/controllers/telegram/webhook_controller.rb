# frozen_string_literal: true

module Telegram
  # Recibe los mensajes desde Telegram y asocia piratas con cuentas de
  # Telegram.
  class WebhookController < Telegram::Bot::UpdatesController
    include Telegram::Bot::UpdatesController::TypedUpdate

    MESSAGES = %w[aha what hi haha].freeze

    def message(_)
      respond_with :message, text: t('.' + MESSAGES.sample)
    end

    # Recibe un token, lo busca y asocia las cuentas
    def start!(token = nil)
      unless token
        respond_with :message, text: t('.need_token', token_link: vincular_a_telegram_url)
        return
      end

      unless (pirata = Pirata.find_by(telegram_token: token))
        respond_with :message, text: t('.couldnt_find')
        return
      end

      pirata.update_attribute :telegram_user, payload.from.id

      respond_with :message, text: t('.hi', pirata: pirata.nick)
    end

    private

    def vincular_a_telegram_url
      "https://#{ENV.fetch('APP_HOST', 'lumi.partidopirata.com.ar')}/#/vincularATelegram"
    end
  end
end
