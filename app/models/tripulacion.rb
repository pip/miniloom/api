# frozen_string_literal: true

# La tripulación de una barca
class Tripulacion < ApplicationRecord
  belongs_to :barca
  belongs_to :pirata
end
