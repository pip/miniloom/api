# frozen_string_literal: true

# Los consensos son textos sobre los que las piratas establecen
# posiciones
class Consenso < ApplicationRecord
  # Es escrito por una pirata
  belongs_to :pirata
  # Agrupa muchas posiciones
  has_many :posiciones
  # Pertenece a una barca
  belongs_to :barca
end
