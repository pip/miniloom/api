# frozen_string_literal: true

# Una posición sobre un consenso
class Posicion < ApplicationRecord
  # Tiene alguno de estos estados.
  #
  # TODO: Poder definirlos por configuración en lugar de hardcodearlos,
  # pero al menos por ahora los podemos cambiar acá y no por todos lados
  # como pasaba en Loomio
  ESTADOS = %w[compromiso a_favor en_contra bloqueo indiferente].freeze

  # Es hecha por una pirata
  belongs_to :pirata
  # Sobre un consenso
  belongs_to :consenso

  # Solo puede tener alguno de los estados definidos
  validates :estado, inclusion: { in: Posicion::ESTADOS }

  # Obtener las últimas posiciones por cada pirata.  Para no hacer
  # consultas complejas, obtenemos primero los IDs de las posiciones y
  # luego traemos solo esas.
  scope :ultimas, lambda {
    ids = select('id, max(created_at) as created_at')
          .group(:pirata_id)
          .collect(&:id)

    where(id: ids)
  }
end
