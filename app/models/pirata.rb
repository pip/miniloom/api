# frozen_string_literal: true

# Las piratas son las usuarias empoderadas del miniloom
class Pirata < ApplicationRecord
  # Puede proponer consensos
  has_many :consensos
  # Y tener posiciones
  has_many :posiciones

  has_many :webpush_subscriptions

  # Puede participar en barcas
  has_many :tripulaciones
  has_many :barcas, through: :tripulaciones

  # Y además una contraseña segura :P
  has_secure_password
  # Una por correo
  validates :email, presence: true, uniqueness: true
  validates :nick, presence: true, uniqueness: true

  scope :todas_menos, ->(pirata) { where.not(id: pirata) }

  before_create :telegram_token!

  # Devuelve la URL de Telegram
  # @return [String]
  def telegram_url
    'https://t.me/' + Telegram.bots[:default].username + '?start=' + telegram_token
  end

  # Asigna un token a cada pirata si no lo tiene
  # @return [TrueClass] Los callbacks tienen que terminar en true
  def telegram_token!
    self.telegram_token = SecureRandom.hex unless telegram_token

    true
  end
end
