# frozen_string_literal: true

# Las piratas abordan barcas a través de tripulaciones
class CreateTripulaciones < ActiveRecord::Migration[5.2]
  def change
    create_table :tripulaciones do |t|
      t.timestamps
      t.belongs_to :barca, index: true
      t.belongs_to :pirata, index: true
    end
  end
end
