# frozen_string_literal: true

# Crear las suscripciones por webpush
class CreateWebpushSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :webpush_subscriptions do |t|
      t.timestamps
      t.belongs_to :pirata, index: true
      t.string :auth
      t.string :p256dh
      t.string :endpoint

      t.index %i[auth p256dh endpoint], unique: true
    end
  end
end
