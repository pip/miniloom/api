# frozen_string_literal: true

# Agrega todos los consensos sueltos a la asamblea general
class AddConsensosToAsambleaGeneral < ActiveRecord::Migration[5.2]
  def up
    return unless Rails.env == 'production'

    barca = Barca.find_or_create_by(nombre: 'Asamblea General')

    Consenso.where(barca_id: nil).find_each do |consenso|
      barca.consensos << consenso
    end

    barca.save
  end

  def down; end
end
