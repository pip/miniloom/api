# frozen_string_literal: true

# Los consensos pertenecen a barcas
class AddBarcaToConsenso < ActiveRecord::Migration[5.2]
  def change
    add_belongs_to :consensos, :barca, index: true
  end
end
