# frozen_string_literal: true

# Crear la tabla de posiciones
class CreatePosiciones < ActiveRecord::Migration[5.2]
  def change
    create_table :posiciones do |t|
      t.timestamps
      t.belongs_to :consenso, index: true
      t.belongs_to :pirata, index: true
      t.string :comentario
      t.string :estado
    end
  end
end
