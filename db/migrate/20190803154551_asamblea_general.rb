# frozen_string_literal: true

# Si no hay una barca, creamos una asamblea general!
class AsambleaGeneral < ActiveRecord::Migration[5.2]
  def up
    return if Barca.all.count.positive?

    barca = Barca.create(nombre: 'Asamblea General',
                         descripcion: 'En la asamblea general tomamos todas las decisiones que no pasan por barcas.')

    Pirata.all.find_each do |pirata|
      barca.piratas << pirata
    end

    barca.save
  end

  def down
    Barca.find_by(nombre: 'Asamblea General').destroy
  end
end
