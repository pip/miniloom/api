# frozen_string_literal: true

# Crear la tabla de consensos
class CreateConsensos < ActiveRecord::Migration[5.2]
  def change
    create_table :consensos do |t|
      t.timestamps
      t.belongs_to :pirata, index: true
      t.string :titulo
      t.text :texto
    end
  end
end
