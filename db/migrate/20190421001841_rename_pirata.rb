# frozen_string_literal: true

# Renombrar la tabla porque nos olvidamos de las inflexiones
class RenamePirata < ActiveRecord::Migration[5.2]
  def change
    rename_table :pirata, :piratas
  end
end
