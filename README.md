# Miniloom API

**Antes que nada solicitar la "master key" de Rails a otrx pirata
y colocarla en `config/master.key`.**

Si estás forkeando o solo trabajando localmente, podés eliminar
`config/credentials.yml.enc` y volverlo a crear con:

```bash
bundle exec rails credentials:edit
secret_key_base: "Reemplazar con bundle exec rake secret"
vapid:
  public_key: "Correr bundle exec rake webpush:vapid"
  private_key: "Correr bundle exec rake webpush:vapid"
```

## Desarrollo

* Instalar las herramientas de compilación de la distro
  (`build-essential`, `base-devel`, etc.)

* Instalar Ruby 2.6.5 usando [rbenv](https://github.com/rbenv/rbenv)
  y [ruby-build](https://github.com/fauno/ruby-build)

* Con Ruby 2.6.5 activado, instalar bundler con `gem install bundler`

* Clonar este repositorio y dentro del directorio, correr `bundle` para
  instalar las gemas.

* Crear o actualizar la base de datos con `bundle exec rake db:migrate`

* Correr el servidor de prueba con `bundle exec rails server -b 0.0.0.0`

## Despliegue

### Manual

* Generar una llave SSH con `ssh-keygen -t ed25519` (la contraseña es
  opcional)

* Solicitar a otra pirata que agregue tu llave pública SSH
  (`~/.ssh/id_ed25519.pub` por ejemplo) en `lumi@partidopirata.com.ar`.

* Correr `bundle exec cap production deploy`

### Automático (CI/CD)

* Editar `.gitlab-ci.yml` si es necesario

* Generar una llave privada SSH para el deploy y convertirla a una sola
  línea para que Gitlab la acepte.

```bash
ssh-keygen -t ed25519 -f ~/.ssh/lumi
# Extraer la llave privada a una sola línea
cat ~/.ssh/lumi | tail -n +2 | head -n -1 | tr -d "\n"
```

* En [0xacab](https://0xacab.org/) (o cualquier Gitlab con CI/CD
  habilitado) ir a Settings > CI/CD > Variables y crear las variables
  protegidas y enmascaradas:

  RAILS_MASTER_KEY: con el contenido de `config/master.key`

  SSH_PRIVATE_KEY: la línea de la llave privada SSH

* Agregar la llave privada a `lumi@partidopirata.com.ar` para que GL
  pueda ingresar.

```bash
ssh-copy-id -i ~/.ssh/lumi lumi@partidopirata.com.ar
```
